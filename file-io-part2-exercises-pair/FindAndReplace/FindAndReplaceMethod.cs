﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindAndReplace
{
    public class FindAndReplaceMethod
    {
        public static void ReplaceWord()
        {
            Console.WriteLine("Please enter a search phrase.");
            string searchPhrase = Console.ReadLine();

            Console.WriteLine("Please enter the phrase you would like to replace your search phrase with.");
            string replacePhrase = Console.ReadLine();

            Console.WriteLine("Please enter a source file path: (Must be an existing path)");
            string fileInput = Console.ReadLine();

            Console.WriteLine("Please enter the destination file path.");
            string destinationInput = Console.ReadLine();

            try
            {
                if (!File.Exists(fileInput) || (!Directory.Exists(destinationInput)))
                {
                    Console.WriteLine("File path does not exist. Please try again.");
                    Console.ReadLine();
                    ReplaceWord();
                }
                else
                {
                    string fileName = "output.txt";
                    destinationInput = Path.Combine(destinationInput, fileName);

                    using (StreamReader sr = new StreamReader(fileInput))
                    {
                        using (StreamWriter sw = new StreamWriter(destinationInput))
                            while (!sr.EndOfStream)
                            {
                                string line = sr.ReadLine();
                                string fixedLine = line.Replace(searchPhrase, replacePhrase);
                                sw.WriteLine(fixedLine);
                            }
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found.");
            }


           
        }
    }
}

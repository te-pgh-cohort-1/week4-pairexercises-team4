﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace file_io_part1_exercises_pair
{
    public static class FilePath
    {
        public static void ReadAlice()
        {
            string directory = Environment.CurrentDirectory;
            string filename = @"Book\alices_adventures_in_wonderland.txt";
            string fullPath = Path.Combine(directory, filename);
            //List<string> allWords = new List<string>();
            try
            {
                using (StreamReader sr = new StreamReader(fullPath))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadToEnd();
                        string[] words = line.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                        char[] delimeteres = { '.', '!', '?' };
                        string[] sentences = line.Split(delimeteres, StringSplitOptions.RemoveEmptyEntries);
                        
                       // allWords.AddRange(words);
                        int numberOfWords = words.Length;
                        Console.WriteLine($"{numberOfWords}" + " words");

                        int numberOfSentences = sentences.Length;
                        Console.WriteLine($"{numberOfSentences}" + " sentences");
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Error reading the file");
                Console.WriteLine(e.Message);
            }
        }
    }
}

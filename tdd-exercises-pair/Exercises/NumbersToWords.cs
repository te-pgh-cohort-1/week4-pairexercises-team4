﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Exercises
{
    public class NumbersToWords
    {
        public string NumberWords (int number)
        {
            string words = "";
            string[] unitsArray = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            string[] tensArray = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

            if (number == 0)
            {
                return "zero";
            }
            if (number < 0)
            {
                return "negative" + NumberWords(Math.Abs(number));
            }
            if ((number/1000) > 0)
            {
                words += NumberWords(number / 1000) + " thousand and ";
                number %= 1000;
            }
            if ((number/100) > 0)
            {
                words += NumberWords(number / 100) + " hundred and";
                number %= 100;
            }
            if (number > 0)
            {
                if (words != "")
                    words += " ";

                if (number < 20)
                {
                    words += unitsArray[number];
                }
                else
                {
                    words += tensArray[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsArray[number % 10];
                }
            }
            return words;











        }











    }
}

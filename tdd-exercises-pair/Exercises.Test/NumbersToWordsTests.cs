﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Test
{
    [TestClass]
    public class NumbersToWordsTests
    {
        public int number = 0;
        public NumbersToWords loops = new NumbersToWords();

        [TestMethod]
        public void OneDigit()
        {
            int number = 1;
            string result = loops.NumberWords(number);
            Assert.AreEqual("one", result, "1 should return one");
        }

        [TestMethod]
        public void TwoDigit()
        {
            int number = 23;
            string result = loops.NumberWords(number);
            Assert.AreEqual("twenty-three", result, "23 should return twenty-three");
        }

        [TestMethod]
        public void ThreeDigit()
        {
            int number = 345;
            string result = loops.NumberWords(number);
            Assert.AreEqual("three hundred and forty-five", result, "345 should return three hundred and forty-five");
        }

        [TestMethod]
        public void FourDigit()
        {
            int number = 1234;
            string result = loops.NumberWords(number);
            Assert.AreEqual("one thousand and two hundred and thirty-four", result, "1234 should return one thousand and two hundred and thirty-four");
        }

        [TestMethod]
        public void FiveDigit()
        {
            int number = 12345;
            string result = loops.NumberWords(number);
            Assert.AreEqual("twelve thousand and three hundred and forty-five", result, "12345 should return twelve thousand and three hundred and forty - five");
        }

        [TestMethod]
        public void SixDigit()
        {
            int number = 123456;
            string result = loops.NumberWords(number);
            Assert.AreEqual("one hundred and twenty-three thousand and four hundred and fifty-six", result, "123456 should return one hundred and twenty-three thousand and four hundred and fifty-six");
        }
    }
}

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;


namespace Exercises.Test
{
    [TestClass]
    public class StringCalculatorTests
    {
        public string numbers;
        public StringCalculator loops = new StringCalculator();

        [TestMethod]
        public void ZeroInput()
        {
            string numbers = "";
            int result = loops.Add(numbers);
            Assert.AreEqual(0, result, "Empty strings should return 0");
        }

        [TestMethod]
        public void OneInput()
        {
            string numbers = "1";
            int result = loops.Add(numbers);
            Assert.AreEqual(1, result, "Input of one number returns that number");
        }

        [TestMethod]
        public void TwoOrMoreInput()
        {
            string numbers = "1,2";
            int result = loops.Add(numbers);
            Assert.AreEqual(3, result, "Input of two or more numbers returns the sum");
        }

        [TestMethod]
        public void NewLineInput()
        {
            string numbers = "1\n2,3";
            int result = loops.Add(numbers);
            Assert.AreEqual(6, result, "Input of new line gets treated as a comma");
        }
    }
}
